Used to automate initial setup for ExpressionEngine. Updates `server_path` column for database table `exp_upload_prefs` to match current working directory.


Conditions:

1. Database must already be setup & populated.
2. database.php must not be empty.
3. specify systemPath in setup/setup.php (default = "panel")