<?php

/**
 * Finds any incorrect server paths and updates them.
 *
 * @author Hyunmin Kim
 */
class ServerPathUpdater
{
    private $pdo;
    private $cwd;

    /**
     * Constructor.
     *
     * @param PDO $pdo
     * @param string $cwd
     */
    public function __construct(PDO $pdo, $cwd)
    {
        $this->pdo = $pdo;
        $this->cwd = $cwd;
    }

    /**
     * Runs program.
     */
    public function run()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM exp_upload_prefs');
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $rowId = $row['id'];
            $serverPath = $row['server_path'];
            $serverPathBase = substr($serverPath, 0, strpos($serverPath, "images"));
            $serverPathRest = substr($serverPath, strpos($serverPath, "images"));

            if ($this->cwd !== $serverPathBase) {
                echo "Found incorrect server path: $serverPath" . PHP_EOL;

                $newServerPath = $this->cwd . $this->getLinuxSupportedPath($serverPathRest);
                $this->updateServerPath($this->pdo, $rowId, $newServerPath);
            }
        }
    }

    /**
     * Returns linux supported path string.
     */
    private function getLinuxSupportedPath($path)
    {
        return str_replace("\\", "/", $path);
    }

    /**
     * Updates server path with correct server path.
     */
    private function updateServerPath($pdo, $rowId, $newServerPath)
    {
        $stmt = $pdo->prepare('UPDATE exp_upload_prefs SET server_path = :server_path WHERE id = :id');
        $stmt->execute(array(
            ':id'           =>  $rowId,
            ':server_path'  =>  $newServerPath,
        ));

        echo "Updated to: $newServerPath\n\n";
    }
}
