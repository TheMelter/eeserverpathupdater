<?php
// Run from project root.
// Ex: php setup/setup.php

require_once("./setup/DatabaseParser.php");
require_once("./setup/ServerPathUpdater.php");

$systemPath = "panel";
$parser = new DatabaseParser($systemPath);
$database = $parser->run();

try {
    $pdo = new PDO(
        'mysql:' .
        'host='     . $database['hostname'] . ';' .
        'dbname='   . $database['database'],
        $database['username'],
        $database['password']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $cwd = getcwd() . DIRECTORY_SEPARATOR;

    $serverPathUpdater = new ServerPathUpdater($pdo, $cwd);
    $serverPathUpdater->run();

} catch (PDOException $e) {
    exit('ERROR: ' . $e->getMessage() . PHP_EOL);
}

