<?php

/**
 * Parses database file and retrieves information needed for PDO.
 *
 * @author Hyunmin Kim
 */
class DatabaseParser
{
    private $systemPath;
    private $databaseConfigFile;
    private $rawContent;

    /**
     * Constructor.
     *
     * @param string $systemPath
     */
    public function __construct($systemPath)
    {
        $this->systemPath = $systemPath;
        $this->databaseConfigFile = './' . $systemPath . '/expressionengine/config/database.php';

        $this->loadRawContent();
    }

    /**
     * Read database.php contents into rawContent as string.
     */
    private function loadRawContent()
    {
        if (!file_exists($this->databaseConfigFile)) {
            exit("No database.php found at: " . $databaseConfigFile . PHP_EOL);
        }

        $this->rawContent = file_get_contents($this->databaseConfigFile);
    }

    /**
     * Returns array of database information.
     *
     * @return array $database
     */
    public function run()
    {
        $pattern = "/\['(hostname|username|password|database)'\]\s*\=\s*'(.+?)'/";
        preg_match_all($pattern, $this->rawContent, $matches);

        $database = array();

        // Make sure all 4 fields are present or halt program.
        if (count($matches[1]) !== 4 || count($matches[2]) !== 4) {
            exit("Could not parse database.php properly. Check contents for integrity." . PHP_EOL);
        }

        // Setting database keys and values. IE: hostname => localhost
        for ($i = 0; $i < count($matches[1]); $i++) {
            $database[$matches[1][$i]] = $matches[2][$i];
        }

        return $database;
    }
}
